# major.minor.patch
VERSION = (3, 0, 1)

__version__ = '.'.join(str(x) for x in VERSION)
