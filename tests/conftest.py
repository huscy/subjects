import pytest
import random
import string

from model_bakery import baker
from rest_framework.test import APIClient


# model_bakery does not know how to deal with phonenumber-field, hence we provide some custom values
def gen_phonenumber():
    return '+4930' + ''.join(random.choices(string.digits, k=6))


baker.generators.add('phonenumber_field.modelfields.PhoneNumberField', gen_phonenumber)


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Donna', last_name='Wetter')


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
def contact():
    return baker.make('subjects.Contact', display_name='John Doe', first_name='John',
                      last_name='Doe', email='john.doe@do.es')


@pytest.fixture
def subject(contact):
    return baker.make('subjects.Subject', contact=contact)


@pytest.fixture
def legal_representative(subject):
    contact = baker.make('subjects.Contact')
    subject.legal_representatives.add(contact)
    return contact
