from datetime import date

import pytest
from freezegun import freeze_time
from model_bakery import baker

pytestmark = pytest.mark.django_db


@freeze_time('2000-01-01')
@pytest.mark.parametrize('date_of_birth,age_in_years', [
    (date(1999, 10, 1), 0),
    (date(1998, 1, 1), 2),
    (date(1998, 1, 2), 1),
    (date(1980, 1, 1), 20),
])
def test_age_in_years(date_of_birth, age_in_years):
    subject = baker.prepare('subjects.Subject', contact__date_of_birth=date_of_birth)

    assert subject.age_in_years == age_in_years


def test_str_method():
    subject = baker.prepare('subjects.Subject', contact__display_name='Donna Wetter')

    assert 'Donna Wetter' == str(subject)
