import pytest
from model_bakery import baker

pytestmark = pytest.mark.django_db


def test_str_method():
    contact = baker.prepare('subjects.Contact', display_name='Dr. Hans Wurst')
    assert str(contact) == 'Dr. Hans Wurst'


def test_is_assigned_to_subjects_as_subject(subject):
    contact = subject.contact
    assert contact.is_assigned_to_subjects is False


def test_is_assigned_to_subjects_as_legal_representative(legal_representative):
    assert legal_representative.is_assigned_to_subjects is True


def test_is_subject_as_subject(subject):
    contact = subject.contact
    assert contact.is_subject is True


def test_is_subject_as_legal_representative(legal_representative):
    assert legal_representative.is_subject is False
