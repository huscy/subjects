import pytest
from model_bakery import baker

from huscy.subjects.serializers import (ContactSerializer, SubjectSerializer,
                                        LegalRepresentativeSerializer)

pytestmark = pytest.mark.django_db


def test_expose_contact_data(subject):
    data = SubjectSerializer(subject).data

    assert data['contact'] == ContactSerializer(subject.contact).data


def test_expose_legal_representatives(subject):
    contacts = baker.make('subjects.Contact', _quantity=2)
    subject.legal_representatives.add(*contacts)
    subject.save()

    data = SubjectSerializer(subject).data

    contacts = sorted(contacts, key=lambda g: (g.last_name.lower(), g.first_name.lower()))
    assert data['legal_representatives'] == LegalRepresentativeSerializer(contacts, many=True).data


def test_expose_empty_list_unless_any_legal_representative_is_assigned_to_subject(subject):
    data = SubjectSerializer(subject).data

    assert data['legal_representatives'] == []
