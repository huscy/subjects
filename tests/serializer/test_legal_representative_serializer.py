import pytest

from huscy.subjects.serializers import LegalRepresentativeSerializer

pytestmark = pytest.mark.django_db


def test_expose_contact_id(legal_representative):
    data = LegalRepresentativeSerializer(legal_representative).data

    assert data['id'] == str(legal_representative.id)
