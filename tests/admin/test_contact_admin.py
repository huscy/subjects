import pytest
from model_bakery import baker

from django.contrib.admin.sites import AdminSite

from huscy.subjects.admin import ContactAdmin
from huscy.subjects.models import Contact


@pytest.fixture
def contact():
    return baker.prepare('subjects.Contact', country='DE', city='Berlin', postal_code='12345',
                         street='Hauptstraße 15')


@pytest.fixture
def contact_admin():
    return ContactAdmin(model=Contact, admin_site=AdminSite())


def test_address(contact_admin, contact):
    assert contact_admin.address(contact) == 'DE-12345 Berlin, Hauptstraße 15'
