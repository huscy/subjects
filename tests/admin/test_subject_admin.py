import pytest
from model_bakery import baker

from django.contrib.admin.sites import AdminSite

from huscy.subjects.admin import SubjectAdmin
from huscy.subjects.models import Subject

pytestmark = pytest.mark.django_db


@pytest.fixture
def subject_admin():
    return SubjectAdmin(model=Subject, admin_site=AdminSite())


def test_display_name(subject, subject_admin):
    assert subject_admin._display_name(subject=subject) == 'John Doe'


def test_date_of_birth(subject, subject_admin):
    assert subject_admin._date_of_birth(subject) == subject.contact.date_of_birth


def test_legal_representatives(subject, subject_admin):
    def get_link(contact):
        url = f'/admin/subjects/contact/{contact.id}/change/'
        return f'<a href="{url}">{contact.display_name}</a>'

    assert subject_admin._legal_representatives(subject) == ''

    contact1 = baker.make('subjects.Contact', last_name='contact1_last_name')
    subject.legal_representatives.add(contact1)

    assert subject_admin._legal_representatives(subject) == get_link(contact1)

    contact2 = baker.make('subjects.Contact', last_name='contact2_last_name')
    subject.legal_representatives.add(contact2)

    assert subject_admin._legal_representatives(subject) == (
        f'{get_link(contact1)}, {get_link(contact2)}')
