Feature: add legal representatives

	Scenario: add legal representative as admin user
		Given I am admin user

		When I try to add a legal representative

		Then I get status code 201

	Scenario: add legal representative as normal user with change_subject permission
		Given I am normal user
		And I have change_subject permission

		When I try to add a legal representative

		Then I get status code 201

	Scenario: add legal representative as normal user
		Given I am normal user

		When I try to add a legal representative

		Then I get status code 403

	Scenario: add legal representative as anonymous user
		Given I am anonymous user

		When I try to add a legal representative

		Then I get status code 403
