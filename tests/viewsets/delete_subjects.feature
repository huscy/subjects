Feature: delete subjects

	Scenario: delete subject as admin user
		Given I am admin user
		When I try to delete a subject
		Then I get status code 204

	Scenario: delete subject as normal user with delete_subject permission
		Given I am normal user
		And I have delete_subject permission
		When I try to delete a subject
		Then I get status code 204

	Scenario: delete subject as normal user
		Given I am normal user
		When I try to delete a subject
		Then I get status code 403

	Scenario: delete subject as anonymous user
		Given I am anonymous user
		When I try to delete a subject
		Then I get status code 403
