import urllib
from datetime import date

import pytest
from model_bakery import baker

from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse

pytestmark = pytest.mark.django_db


def test_list_subjects(client, user):
    baker.make('subjects.Subject', is_child=True, _quantity=5)
    baker.make('subjects.Subject', _quantity=15)

    user.user_permissions.add(Permission.objects.get(codename='view_subject'))

    response = list_subjects(client)

    assert response.json()['count'] == 20


@pytest.mark.parametrize('subject_count, result_count', [
    (1, 1),
    (25, 25),
    (26, 25),
    (100, 25),
])
def test_pagination(admin_client, subject_count, result_count):
    baker.make('subjects.Subject', _quantity=subject_count)

    response = list_subjects(admin_client)

    json = response.json()
    assert json['count'] == subject_count
    assert len(json['results']) == result_count


def test_paginated_item_count(admin_client):
    baker.make('subjects.Subject', _quantity=20)

    response = list_subjects(admin_client, dict(count=5))

    json = response.json()
    assert json['count'] == 20
    assert len(json['results']) == 5


@pytest.mark.parametrize('search_string, result_count', [
    ('D', 1),
    ('Donna', 1),
    ('Donna Wetter', 1),
    ('Wetter', 1),
    ('W', 1),
    ('Regen', 0),
])
def test_search_by_name(admin_client, search_string, result_count):
    baker.make('subjects.Subject', contact__display_name='Donna Wetter')

    response = list_subjects(admin_client, dict(search=search_string))

    json = response.json()
    assert json['count'] == result_count


@pytest.mark.parametrize('date_of_birth, result_count', [
    ('2000-01-01', 1),
    ('2004-12-24', 0),
])
def test_search_by_date_of_birth(admin_client, date_of_birth, result_count):
    baker.make('subjects.Subject', contact__date_of_birth=date(2000, 1, 1))

    response = list_subjects(admin_client, dict(search=date_of_birth))

    json = response.json()
    assert json['count'] == result_count


def test_limit_queryset_by_settings_variable(admin_client):
    baker.make('subjects.Subject', _quantity=501)

    response = list_subjects(admin_client)

    assert response.json()['count'] == 400


def test_limit_queryset_with_default_value_due_to_absence_of_settings_variable(settings,
                                                                               admin_client):
    del settings.HUSCY

    baker.make('subjects.Subject', _quantity=501)

    response = list_subjects(admin_client)

    assert response.json()['count'] == 500


def test_limit_queryset_by_new_value_for_settings_variable(settings, admin_client):
    settings.HUSCY = {'subjects': {'subject_viewset_max_result_count': 250}}

    baker.make('subjects.Subject', _quantity=501)

    response = list_subjects(admin_client)

    assert response.json()['count'] == 250


def list_subjects(client, params=None):
    url = reverse('subject-list')
    if params:
        url += '?' + urllib.parse.urlencode(params)
    return client.get(url)
