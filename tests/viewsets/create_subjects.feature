Feature: create subjects

	Scenario: create subject as admin user
		Given I am admin user
		When I try to create a subject
		Then I get status code 201

	Scenario: create subject as normal user with add_subject permission
		Given I am normal user
		And I have add_subject permission
		When I try to create a subject
		Then I get status code 201

	Scenario: create subject as normal user
		Given I am normal user
		When I try to create a subject
		Then I get status code 403

	Scenario: create subject as anonymous user
		Given I am anonymous user
		When I try to create a subject
		Then I get status code 403
