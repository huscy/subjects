Feature: update subjects

	Scenario: partial update is not supported
		Given I am admin user
		When I try to partial update a subject
		Then I get status code 405

	Scenario: update subject as admin user
		Given I am admin user
		When I try to update a subject
		Then I get status code 200

	Scenario: update subject as normal user with change_subject permission
		Given I am normal user
		And I have change_subject permission
		When I try to update a subject
		Then I get status code 200

	Scenario: update subject as normal user
		Given I am normal user
		When I try to update a subject
		Then I get status code 403

	Scenario: update subject as anonymous user
		Given I am anonymous user
		When I try to update a subject
		Then I get status code 403
