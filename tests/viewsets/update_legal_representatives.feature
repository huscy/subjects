Feature: update legal representatives

	Scenario: partial update is not supported
		Given I am admin user

		When I try to partial update a legal representative

		Then I get status code 405

	Scenario: update legal representative as admin user
		Given I am admin user

		When I try to update a legal representative

		Then I get status code 200

	Scenario: update legal representative as normal user with change_subject permission
		Given I am normal user
		And I have change_subject permission

		When I try to update a legal representative

		Then I get status code 200

	Scenario: update legal representative as normal user
		Given I am normal user

		When I try to update a legal representative

		Then I get status code 403

	Scenario: update legal representative as anonymous user
		Given I am anonymous user

		When I try to update a legal representative

		Then I get status code 403
