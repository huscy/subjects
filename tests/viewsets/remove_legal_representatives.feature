Feature: remove legal representatives

	Scenario: remove legal representative as admin user
		Given I am admin user

		When I try to remove a legal representative

		Then I get status code 204

	Scenario: remove legal representative as normal user with change_subject permission
		Given I am normal user
		And I have change_subject permission

		When I try to remove a legal representative

		Then I get status code 204

	Scenario: remove legal representative as normal user
		Given I am normal user

		When I try to remove a legal representative

		Then I get status code 403

	Scenario: remove legal representative as anonymous user
		Given I am anonymous user

		When I try to remove a legal representative

		Then I get status code 403
