Feature: view subjects

	Scenario: list subjects as admin user
		Given I am admin user
		When I try to list subjects
		Then I get status code 200

	Scenario: retrieve subject as admin user
		Given I am admin user
		When I try to retrieve a subject
		Then I get status code 200

	Scenario: list subjects as normal user with view_subject permission
		Given I am normal user
		And I have view_subject permission
		When I try to list subjects
		Then I get status code 200

	Scenario: retrieve subject as normal user with view_subject permission
		Given I am normal user
		And I have view_subject permission
		When I try to retrieve a subject
		Then I get status code 200

	Scenario: list subjects as normal user
		Given I am normal user
		When I try to list subjects
		Then I get status code 403

	Scenario: retrieve subject as normal user
		Given I am normal user
		When I try to retrieve a subject
		Then I get status code 403

	Scenario: list subjects as anonymous user
		Given I am anonymous user
		When I try to list subjects
		Then I get status code 403

	Scenario: retrieve subject as anonymous user
		Given I am anonymous user
		When I try to retrieve a subject
		Then I get status code 403
