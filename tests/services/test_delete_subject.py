import pytest
from model_bakery import baker

from huscy.subjects.models import Contact, Subject
from huscy.subjects.services import delete_subject

pytestmark = pytest.mark.django_db


def test_delete_subject(subject):
    assert Subject.objects.count() == 1
    assert Contact.objects.count() == 1

    delete_subject(subject)

    assert Contact.objects.count() == 0
    assert Subject.objects.count() == 0


def test_delete_legal_representatives(subject):
    contacts = baker.make(Contact, _quantity=2)
    subject.legal_representatives.add(*contacts)

    assert Contact.objects.count() == 3

    delete_subject(subject)

    assert Contact.objects.count() == 0


def test_dont_delete_legal_representative_if_it_is_assigned_to_another_subject(
        subject, legal_representative):
    other_subject = baker.make(Subject)
    other_subject.legal_representatives.add(legal_representative)

    assert Contact.objects.count() == 3  # 2 subjects and one legal_representative

    delete_subject(subject)

    assert Contact.objects.count() == 2  # other_subject and legal_representative
