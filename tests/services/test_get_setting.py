import pytest

from huscy.subjects.services import _get_setting

DEFAULT_VALUE = 21


@pytest.fixture(autouse=True)
def temporary_settings(settings):
    settings.HUSCY = {
        'subjects': {
            'age_of_majority': 18,
            'subject_viewset_max_result_count': 400,
        }
    }


def test_get_setting():
    assert 18 == _get_setting('age_of_majority', DEFAULT_VALUE)


def test_global_huscy_settings_variable_does_not_exist(settings):
    del settings.HUSCY

    assert DEFAULT_VALUE == _get_setting('age_of_majority', DEFAULT_VALUE)


def test_app_name_does_not_exist_in_global_huscy_settings(settings):
    del settings.HUSCY['subjects']

    assert DEFAULT_VALUE == _get_setting('age_of_majority', DEFAULT_VALUE)


def test_key_does_not_exist_in_app_settings(settings):
    del settings.HUSCY['subjects']['age_of_majority']

    assert DEFAULT_VALUE == _get_setting('age_of_majority', DEFAULT_VALUE)
