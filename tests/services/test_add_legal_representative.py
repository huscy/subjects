import pytest
from model_bakery import baker

from huscy.subjects.services import add_legal_representative

pytestmark = pytest.mark.django_db


def test_add_legal_representative(subject):
    contact = baker.make('subjects.Contact')

    assert subject.legal_representatives.count() == 0

    add_legal_representative(subject, contact)

    assert list(subject.legal_representatives.all()) == [contact]


def test_add_subject_itself_as_legal_representative(subject):
    assert subject.legal_representatives.count() == 0

    with pytest.raises(ValueError) as e:
        add_legal_representative(subject, subject.contact)

    assert str(e.value) == ('Cannot add contact as legal_representative because it\'s the '
                            'subject itself!')
    assert subject.legal_representatives.count() == 0
