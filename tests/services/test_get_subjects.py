import pytest
from model_bakery import baker

from huscy.subjects.services import get_subjects

pytestmark = pytest.mark.django_db


def test_get_subjects_without_arguments():
    baker.make('subjects.Subject', _quantity=20)

    assert 20 == len(get_subjects())


def test_include_children():
    baker.make('subjects.Subject', is_child=True, _quantity=5)
    baker.make('subjects.Subject', _quantity=20)

    assert 25 == len(get_subjects(include_children=True))


def test_exclude_children():
    baker.make('subjects.Subject', is_child=True, _quantity=5)
    baker.make('subjects.Subject', _quantity=20)

    assert 20 == len(get_subjects())


def test_number_of_queries(django_assert_max_num_queries):
    for _ in range(5):
        contacts = baker.make('subjects.Contact', _quantity=2)
        baker.make('subjects.Subject', legal_representatives=contacts)

    with django_assert_max_num_queries(2):
        # 1 query: select related('contact')
        # 1 query: prefetch_related('legal_representatives')
        results = list(get_subjects(include_children=True))

    with django_assert_max_num_queries(0):
        # no extra queries because of prefetch_related
        [contact.id for subject in results for contact in subject.legal_representatives.all()]
