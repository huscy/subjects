import pytest
from model_bakery import baker

from huscy.subjects.models import Contact, Subject
from huscy.subjects.services import remove_legal_representative

pytestmark = pytest.mark.django_db


def test_legal_representative_is_assigned_to_one_subject(subject, legal_representative):
    assert subject.legal_representatives.count() == 1
    assert legal_representative.subjects.count() == 1

    remove_legal_representative(subject, legal_representative)

    assert subject.legal_representatives.count() == 0
    assert not Contact.objects.filter(pk=legal_representative.pk).exists()


def test_legal_representative_is_assigned_to_multiple_subjects(subject, legal_representative):
    another_subject = baker.make(Subject)
    another_subject.legal_representatives.add(legal_representative)

    assert subject.legal_representatives.count() == 1
    assert another_subject.legal_representatives.count() == 1
    assert legal_representative.subjects.count() == 2

    remove_legal_representative(subject, legal_representative)

    assert subject.legal_representatives.count() == 0
    assert another_subject.legal_representatives.count() == 1
    assert legal_representative.subjects.count() == 1
    assert Contact.objects.filter(pk=legal_representative.pk).exists()


def test_dont_delete_legal_representative_if_it_is_subject_itself(subject, legal_representative):
    baker.make(Subject, contact=legal_representative)

    remove_legal_representative(subject, legal_representative)

    assert Subject.objects.filter(contact=legal_representative).exists()
