# subjects

* `/api/subjects/` **[GET]**
    * list subject
        * result list is paginated
            * item per page: 25
            * max items per page: 100
            * access page with `?page=<page_number>`
            * change items per page with `?count=<item_count>`
        * search subjects with `?search=<query_string>`
            * search fields are: `contact.display_name`, `contact.date_of_birth`
        * order with `?ordering=<field_name>`
            * order by: `contact__date_of_birth`, `contact__first_name`, `contact__gender`, `contact__last_name`
    * required permissions: `view_subject`
* `/api/subjects/` **[POST]**
    * create new subject
    * required permissions: `add_subject`
* `/api/subjects/<subject_id>/` **[DELETE]**
    * delete subject
    * required permissions: `delete_subject`
* `/api/subjects/<subject_id>/` **[GET]**
    * retrieve subject
    * required permissions: `view_subject`
* `/api/subjects/<subject_id>/` **[PUT]**
    * update subject together with contact data
    * required permissions: `change_subject`

## Guardians
* `/api/subjects/<subject_id>/gardians/` **[POST]**
    * add a guardian to subject
    * required permissions: `change_subject`
* `/api/subjects/<subject_id>/gardians/<guardian_id>/` **[DELETE]**
    * removes a guardian from subject
    * required permissions: `change_subject`
* `/api/subjects/<subject_id>/gardians/<guardian_id>/` **[PUT]**
    * update guardian contact data
    * required permissions: `change_subject`

## Inactivity
* `/api/subjects/<subject_id>/inactivity/` **[DELETE]**
    * removes inactivity from subject
    * required permissions: `change_subject`
* `/api/subjects/<subject_id>/inactivity/` **[POST]**
    * creates or updates the inactivity for a subject
    * required permissions: `change_subject`
